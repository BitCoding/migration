<?php
namespace Migrations\Util;

use Bit\Collection\Collection;
use Bit\Utility\Hash;
use ReflectionClass;

/**
 * Utility class used to parse arguments passed to a ``bake migration`` class
 */
class ColumnParser
{

    /**
     * Regex used to parse the column definition passed through the shell
     *
     * @var string
     */
    protected $regexpParseColumn = '/^((?:(\w*(?:(:|))))+):(\w*)$/';

    /**
     * Regex used to parse the field type and length
     *
     * @var string
     */
    protected $regexpParseField = '/(\w+)\[(\d+|\w+)\]/';

    /**
     * Parses a list of arguments into an array of fields
     *
     * @param array $arguments A list of arguments being parsed
     * @return array
     */
    public function parseFields($arguments)
    {
        $fields = [];
        $old = [];
        
        $dataSet = false;
        if(!empty($arguments)){
            $dataSet = true;
            $old =$arguments;
        }
        
        if($dataSet && !$arguments){
            var_dump([$old,$arguments]);

            die();
        }

        foreach ($arguments as $field) {
            $_fields = explode(":",$field);

            $indexType = null;
            if(count($_fields) === 2) {
            }else if(count($_fields) >= 3) {
                $indexType = array_pop($_fields);
            }
            $_fields = array_chunk($_fields,2);
            
            foreach ($_fields as $field){
                list($field,$type) = $field;

                $auto = true;
                if (preg_match($this->regexpParseField, $type, $matches)) {
                    list($type,$auto) = [$matches[1], $matches[2]];
                    $auto = $auto;
                }
                $typeIsPk = in_array($type, ['primary', 'primary_key']);
                $isPrimaryKey = false;
                
                if (preg_match($this->regexpParseField, $indexType, $matches)) {
                    list($indexType,$auto) = [$matches[1], $matches[2]];
                    $auto = $auto;
                }
                
                $auto = is_string($auto) ? filter_var($auto, FILTER_VALIDATE_BOOLEAN) : $auto;

                if ($typeIsPk || in_array($indexType, ['primary', 'primary_key'])) {
                    $isPrimaryKey = true;

                    if ($typeIsPk) {
                        $type = 'primary';
                    }
                }

                list($type, $length) = $this->getTypeAndLength($field, $type);
                $fields[$field] = [
                    'columnType' => $type,
                    'options' => [
                        'null' => false,
                        'default' => null,
                    ]
                ];

                if ($length !== null) {
                    $fields[$field]['options']['limit'] = $length;
                }

                if (preg_match($this->regexpParseField, $type, $matches)) {
                    return [$matches[1], $matches[2]];
                }

                if ($auto && $isPrimaryKey === true && count($_fields) === 1 && $type === 'integer') {
                    $fields[$field]['options']['autoIncrement'] = true;
                }

            }
        }
        return $fields;
    }

    /**
     * Parses a list of arguments into an array of indexes
     *
     * @param array $arguments A list of arguments being parsed
     * @return array
     */
    public function parseIndexes($arguments)
    {
        $indexes = [];
        foreach ($arguments as $field) {
            $_fields = explode(":",$field);

            $indexType = null;
            if(count($_fields) === 2) {
            }else if(count($_fields) >= 3) {
                $indexType = array_pop($_fields);
            }
            $_fields = array_chunk($_fields,2);
            //
            foreach ($_fields as $field){
                list($field,$type) = $field;
                $indexName = null;
                
                if (preg_match($this->regexpParseField, $type, $matches)) {
                    list($type,$auto) = [$matches[1], $matches[2]];
                }
                if (preg_match($this->regexpParseField, $indexType, $matches)) {
                    list($indexType,$auto) = [$matches[1], $matches[2]];
                }
                if (in_array($type, ['primary', 'primary_key']) ||
                    in_array($indexType, ['primary', 'primary_key']) ||
                    $indexType === null) {
                    continue;
                }

                $indexUnique = false;
                if ($indexType === 'unique') {
                    $indexUnique = true;
                }

                $indexName = $this->getIndexName($field, $indexType, $indexName, $indexUnique);

                if (empty($indexes[$indexName])) {
                    $indexes[$indexName] = [
                        'columns' => [],
                        'options' => [
                            'unique' => $indexUnique,
                            'name' => $indexName,
                        ],
                    ];
                }

                $indexes[$indexName]['columns'] = explode(" ",$field);
            }            
        }

        return $indexes;
    }

    /**
     * Parses a list of arguments into an array of fields composing the primary key
     * of the table
     *
     * @param array $arguments A list of arguments being parsed
     * @return array
     */
    public function parsePrimaryKey($arguments)
    {
        $primaryKey = [];
        foreach ($arguments as $field) {
            $_fields = explode(":",$field);

            $indexType = null;
            if(count($_fields) === 2) {
            }else if(count($_fields) >= 3) {
                $indexType = array_pop($_fields);
            }
            $_fields = array_chunk($_fields,2);
            //
            foreach ($_fields as $field){
                list($field,$type) = $field;
                $auto = null;
                if (preg_match($this->regexpParseField, $type, $matches)) {
                    list($type,$auto) = [$matches[1], $matches[2]];
                }
                if (preg_match($this->regexpParseField, $indexType, $matches)) {
                    list($indexType,$auto) = [$matches[1], $matches[2]];
                }
                if (in_array($type, ['primary', 'primary_key']) || in_array($indexType, ['primary', 'primary_key'])) {
                    $primaryKey[] = $field;
                }
            }
        }

        return $primaryKey;
    }

    /**
     * Returns a list of only valid arguments
     *
     * @param array $arguments A list of arguments
     * @return array
     */
    public function validArguments($arguments)
    {
        $collection = new Collection($arguments);
        return $collection->filter(function ($value, $field) {
            return preg_match($this->regexpParseColumn, $value);
        })->toArray();
    }

    /**
     * Get the type and length of a field based on the field and the type passed
     *
     * @param string $field Name of field
     * @param string $type User-specified type
     * @return array First value is the field type, second value is the field length. If no length
     * can be extracted, null is returned for the second value
     */
    public function getTypeAndLength($field, $type)
    {
        if (preg_match($this->regexpParseField, $type, $matches)) {
            return [$matches[1], $matches[2]];
        }
        $fieldType = $this->getType($field, $type);

        $length = $this->getLength($fieldType);

        return [$fieldType, $length];
    }

    /**
     * Retrieves a type that should be used for a specific field
     *
     * @param string $field Name of field
     * @param string $type User-specified type
     * @return string
     */
    public function getType($field, $type)
    {
        $reflector = new ReflectionClass('Phinx\Db\Adapter\AdapterInterface');
        $collection = new Collection($reflector->getConstants());

        $validTypes = $collection->filter(function ($value, $constant) {
            return substr($constant, 0, strlen('PHINX_TYPE_')) === 'PHINX_TYPE_';
        })->toArray();

        //var_dump($validTypes);
        $fieldType = $type;
        //var_dump([$field, $fieldType]);
        if ($type === null || !in_array($type, $validTypes)) {
            if ($type === 'primary') {
                $fieldType = 'integer';
            } elseif ($field === 'id') {
                $fieldType = 'integer';
            } elseif (in_array($field, ['created', 'modified', 'updated','deleted'])) {
                $fieldType = 'datetime';
            } else {
                $fieldType = 'string';
            }
        }

        return $fieldType;
    }

    /**
     * Returns the default length to be used for a given fie
     *
     * @param string $type User-specified type
     * @return int
     */
    public function getLength($type)
    {
        $length = null;
        if ($type === 'string') {
            $length = 255;
        } elseif ($type === 'integer') {
            $length = 11;
        } elseif ($type === 'biginteger') {
            $length = 20;
        }

        return $length;
    }

    /**
     * Returns the default length to be used for a given fie
     *
     * @param string $field Name of field
     * @param string $indexType Type of index
     * @param string $indexName Name of index
     * @param bool $indexUnique Whether this is a unique index or not
     * @return string
     */
    public function getIndexName($field, $indexType, $indexName, $indexUnique)
    {
        if (empty($indexName)) {
            $indexName = strtoupper('BY_' . $field);
            if ($indexType === 'primary') {
                $indexName = 'PRIMARY';
            } elseif ($indexUnique) {
                $indexName = strtoupper('UNIQUE_' . $field);
            }
        }

        return $indexName;
    }
}
